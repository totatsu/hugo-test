#!/usr/bin/perl
use strict;
$| = 1;

my $time0 = time;
my $time_max = $time0 - 24*60*60;
my $time_min = $time0 - 2*(30*24*60*60);
my $count = 200;

chdir "content";
# foreach my $type ("news", "glossary", "faq") {
foreach my $type ("news", "faq") {
    chdir $type;
    system("rm -r ${type}*");
    foreach my $i (1 .. $count) {
        my $dir = "$type$i";
        mkdir $dir;
        my $md = "$dir/_index.md";
        open(MD, ">", $md) || die;
        &write_md($type, $i);
        close(MD);
        if ($i % 10 == 1) {
            system("echo 'internal' >$dir/htaccess.txt");
        }
    }
    chdir "..";
}
exit 0;

sub random_date {
    my $t = $time_min + rand($time_max - $time_min);
    my @t = localtime($t);
    return sprintf("%04d-%02d-%02d", $t[5]+1900, $t[4]+1, $t[3]);
}

sub random_words {
    my ($w, $max) = @_;
    my @w;
    foreach (1 .. $max) {
        next if rand() <= 0.5;
        $_ = sprintf("%s%d", $w, rand($count)+1);
        push(@w, $_);
    }
    return @w;
}

sub write_md {
    my ($type, $i) = @_;
    my $title = sprintf("%s #%dについて", ucfirst($type), $i);
    my $date = &random_date();
    my @tags = &random_words("タグ", 3);
    if ($type eq "news") {
        if (rand() <= 0.01) {
            push(@tags, "trouble-now", "trouble");
        } elsif (rand() <= 0.3) {
            push(@tags, "trouble");
        } elsif (rand() <= 0.5) {
            push(@tags, "maintenance");
        } else {
            push(@tags, "info");
        }
    }
    my @categories;
    if (0) {
        @categories = &random_words("カテゴリー", 3);
    }
    my @keywords;
    if (0 && $type eq "glossary") {
        @keywords = &random_words("キーワード", 2);
    }
    print MD "---\n";
    printf MD "title: \"%s\"\n", $title;
    printf MD "date: %s\n", $date;
    if (@tags) {
        printf MD "tags: [ %s ]\n", join(", ", @tags);
    }
    if (@categories) {
        printf MD "categories: [ %s ]\n", join(", ", @categories);
    }
    if (@keywords) {
        printf MD "keywords: [ %s ]\n", join(", ", @keywords);
    }
    if (0 && $type eq "news") {
        print MD "news: true\n";
    }
    print MD "---\n";
    print MD "\n";
    if (@tags) {
        printf MD "## タグ\n";
        print MD "\n";
        foreach (@tags) {
            print MD "- $_\n";
        }
        print MD "\n";
    }
    if (@categories) {
        printf MD "## カテゴリー\n";
        print MD "\n";
        foreach (@categories) {
            print MD "- $_\n";
        }
        print MD "\n";
    }
    if (@keywords) {
        printf MD "## キーワード\n";
        print MD "\n";
        foreach (@keywords) {
            print MD "- $_\n";
        }
        print MD "\n";
    }
    printf MD "## テスト文\n";
    print MD "\n";
    print MD "> ", `fortune`;
    print MD "\n";
}

