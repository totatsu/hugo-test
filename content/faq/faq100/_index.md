---
title: "Faq #100について"
date: 2019-12-29
tags: [ タグ63 ]
---

## タグ

- タグ63

## テスト文

> So she went into the garden to cut a cabbage leaf to make an apple pie;
and at the same time a great she-bear, coming up the street pops its head
into the shop. "What! no soap?" So he died, and she very imprudently
married the barber; and there were present the Picninnies, and the Grand
Panjandrum himself, with the little round button at top, and they all
fell to playing the game of catch as catch can, till the gunpowder ran
out at the heels of their boots.
		-- Samuel Foote

