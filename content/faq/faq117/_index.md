---
title: "Faq #117について"
date: 2020-01-05
tags: [ タグ114, タグ170, タグ131 ]
---

## タグ

- タグ114
- タグ170
- タグ131

## テスト文

> Gone With The Wind LITE(tm)
	-- by Margaret Mitchell

	A woman only likes men she can't have and the South gets trashed.

Gift of the Magi LITE(tm)
	-- by O. Henry

	A husband and wife forget to register their gift preferences.

The Old Man and the Sea LITE(tm)
	-- by Ernest Hemingway

	An old man goes fishing, but doesn't have much luck.

