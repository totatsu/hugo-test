---
title: "Faq #146について"
date: 2019-12-17
tags: [ タグ152, タグ187, タグ87 ]
---

## タグ

- タグ152
- タグ187
- タグ87

## テスト文

> Q:	What's the contour integral around Western Europe?
A:	Zero, because all the Poles are in Eastern Europe!

Addendum: Actually, there ARE some Poles in Western Europe, but they
	are removable!

Q:	An English mathematician (I forgot who) was asked by his
	very religious colleague: Do you believe in one God?
A:	Yes, up to isomorphism!

Q:	What is a compact city?
A:	It's a city that can be guarded by finitely many near-sighted
	policemen!
		-- Peter Lax

