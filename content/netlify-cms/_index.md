---
title: "Netlify CMSの利用"
---

Netlify CMSを利用して，Web上で記事のポストができるようにする．

<!--more-->

## Netlify CMSのデモサイトをコピーして作成する

1. GitLabのアカウントを作成
2. <https://www.netlifycms.org> をアクセス
3. "GET STARTED"をクリック
4. "Hugo Site Starter"をクリック
5. "connect to GiLab"をクリック
   - GitLabでログイン
6. "Save & Deploy"をクリック
7. これでコーヒーのサイトができる
8. Netlify CMSを使って記事のポストをできるようにする
   - Netlifyにログイン
   - "Identity" で "Enable Identity"
   - "Identity / Settings and usage"
     - "Registration" を "Invite only"
     - "External providers" で "GitLab" を選択
     - "Git Gateway" で "Enable Git Gateway"
9. 自分をポストできるユーザとして登録する
   - "Identity / Settings and usage"
     - "Invite Users"をクリックし自分のメールアドレスを入力
     - 大学のメールアドレスだと届くのに時間がかかるようだ
     - 届いたメールの "Accept the invite" のURLを開く
     - "Continue with GitLab" をクリック
     - GitLabの "Authorize" をクリック
10. 記事をポストする
    - 自分のWebサイトを開く
    - URLの最後を `/admin/` に変更する
      - "New Post"をクリック
      - 記事を書く (Markdownも利用できる)
      - "Public"をクリック
    - しばらくすると新しい記事が自分のWebサイトに現れる!


