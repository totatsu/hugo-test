---
title: "News #103について"
date: 2019-12-05
tags: [ タグ187, タグ112, maintenance ]
---

## タグ

- タグ187
- タグ112
- maintenance

## テスト文

> For the fashion of Minas Tirith was such that it was built on seven levels,
each delved into a hill, and about each was set a wall, and in each wall
was a gate.
		-- J.R.R. Tolkien, "The Return of the King"

	[Quoted in "VMS Internals and Data Structures", V4.4, when
	 referring to system overview.]


