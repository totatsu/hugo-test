---
title: "News #39について"
date: 2019-12-16
tags: [ タグ143, タグ38, タグ119, info ]
---

## タグ

- タグ143
- タグ38
- タグ119
- info

## テスト文

> Why is it that we rejoice at a birth and grieve at a funeral?  It is because we
are not the person involved.
		-- Mark Twain, "Pudd'nhead Wilson's Calendar"

