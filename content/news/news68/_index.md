---
title: "News #68について"
date: 2019-12-23
tags: [ タグ190, タグ200, タグ187, maintenance ]
---

## タグ

- タグ190
- タグ200
- タグ187
- maintenance

## テスト文

> Q:	How many existentialists does it take to screw in a light bulb?
A:	Two.  One to screw it in and one to observe how the light bulb
	itself symbolizes a single incandescent beacon of subjective
	reality in a netherworld of endless absurdity reaching out toward a
	maudlin cosmos of nothingness.

