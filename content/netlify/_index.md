---
title: "GitLabとNetlifyの利用"
---

GitLabとNetlifyを連携させて自分のWebサイトを作る．

<!--more-->

## このサイトのコピーを作成する

1. [GitLab](https://gitlab.com)のアカウントを作成し，ログイン
   - 右上のアイコンから "Settings" を選択
   - 左のメニューで "Preferences" を選択
   - "Localization / Language" で "日本語" を選択し "Save Changes" をクリック
2. hugo-testをforkする
   - <https://gitlab.com/tamura70/hugo-test>を開く
   - "フォーク"をクリックし自分のアカウントを選択
   - これで hugo-test のコピーができる (プライベートリポジトリ)
3. <https://www.netlify.com> を開く
   - "Sign up"で"GitLab"を選択しログインする
   - "New site from Git"を選択
   - "Continuous Development"で"GitLab"を選択
   - 自分のリポジトリの一覧が表示されるので `hugo-test` を選択
   - "Deploy site"をクリック
   - "Deploys"のタブで構築状況がわかる
4. 自分のWebサイトが構築される!
   - `https://????.netlify.com` でサイトが公開されている (https!)
5. GitLabで編集
   - 左のメニューから "リポジトリ / ファイル" を選択
   - `content` フォルダの `_index.md` を選択
   - "編集" ボタンで編集画面に移り，ファイルを編集
   - 画面下の "Commit Changes" ボタンを押す
   - しばらく待つと，自分のWebサイトの内容が更新される!
   - ついでに以下を行い，リポジトリの親子関係を削除しておく
     - 左のメニューで "設定" を選択
     - "高度な設定" を展開し "Remove fork relationship" をクリック
     - 確認のため "hugo-test" と入力

